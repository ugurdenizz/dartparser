Text(
    "Body 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 17,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 34,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 34,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 34,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 34,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 24,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 24,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 24,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 24,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 3",
    style: TextStyle(
        color: Colors.white,
        fontSize: 21,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 3",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 21,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 3",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 21,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Headline 3",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 21,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "BUTTON 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 1",
    style: TextStyle(
        color: Colors.white,
        fontSize: 11,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 1",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 11,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 1",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 11,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 1",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 11,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Caption 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Subtitle 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 15,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 3",
    style: TextStyle(
        color: Colors.white,
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 3",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 3",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Body 3",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 13,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 2",
    style: TextStyle(
        color: Colors.white,
        fontSize: 10,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 2",
    style: TextStyle(
        color: Color(0xffd4d4d4),
        fontSize: 10,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 2",
    style: TextStyle(
        color: Color(0xffa9a9a9),
        fontSize: 10,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
),
Text(
    "Footnote 2",
    style: TextStyle(
        color: Color(0xff7f7f7f),
        fontSize: 10,
        fontFamily: "SF Compact Rounded",
        fontWeight: FontWeight.w500,
    ),
)

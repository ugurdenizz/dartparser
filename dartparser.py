
#2021 WTFPL – Do What Ever You want License
#Made by Ugur with ♥ for future 3pounds projects
#contact ugur@3pounds.io



import re

#chekc file formatting and return nested

def ParseNestedParen(string, level):
    """
    Return string contained in nested (), indexing i = level
    """
    CountLeft = len(re.findall("\(", string))
    CountRight = len(re.findall("\)", string))
    if CountLeft == CountRight:
        LeftRightIndex = [x for x in zip(
        [Left.start()+1 for Left in re.finditer('\(', string)],
        reversed([Right.start() for Right in re.finditer('\)', string)]))]

    elif CountLeft > CountRight:
        return ParseNestedParen(string + ')', level)

    elif CountLeft < CountRight:
        return ParseNestedParen('(' + string, level)

    return string[LeftRightIndex[level][0]:LeftRightIndex[level][1]]


f = open("parse.dart", "r")

parsedData=ParseNestedParen(f.read(),0)

# get TextStyles

splitlist=parsedData.split("TextStyle")

# print(splitlist[1][:splitlist[1].find("),\nText")])

# get parameter

splitlist=parsedData.split("Text(")

parameter= re.search('"(.*)"', splitlist[1])

# print("parameter value is :")
#
# print(parameter.group())


for i in range(1,len(splitlist)):

    #parametername
    splitlist=parsedData.split("Text(")
    parameter= re.search('"(.*)"', splitlist[i])

    #textstyle
    splitlist=parsedData.split("TextStyle")
    textStyle=splitlist[i][:splitlist[i].find("),\nText")]

    parameterDefinition = "static TextStyle  "+parameter.group().strip('"').replace(" ","")+ "=TextStyle"+textStyle
    print(parameterDefinition)
